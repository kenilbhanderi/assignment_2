import "./assets/css/App.css";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from './components/home/home';

const App = () => {
  return (
    <Switch>
      <Route path='/home'>
        <Home/>
      </Route>
      <Route path='*'>
        <Redirect to='/home'/>
      </Route>
    </Switch>
  );
};

export default App;
