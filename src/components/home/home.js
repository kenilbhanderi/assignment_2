import React, { useEffect, useState } from "react";
import { GAME_API_URL } from "../../assets/constants/url";
import { SORT_BUTTON_TEXT } from "../../assets/constants/constants";
import Table from "../table/table";
import { compare } from "../../utils/sort";

const Home = () => {
  const [gameData, setGameData] = useState([]);
  const [gameDataSecond, setGameDataSecond] = useState([]);

  // for fatch data when page load at start
  useEffect(() => {
    const apiCall = async () => {
      const data = await fetch(GAME_API_URL);
      const arrayOfData = data.json();
      arrayOfData
        .then((response) => {
          let gameDataConst = response;
          gameDataConst.shift();
          for (let i = 0; i < gameDataConst.length; i++) {
            gameDataConst[i] = {
              id: i,
              ...gameDataConst[i],
            };
          }
          setGameData(gameDataConst);
          setGameDataSecond(gameDataConst);
        })
        .catch((error) => {
          alert(error.message);
        });
    };

    apiCall();
  }, []);

  // sort button function handler
  const onClickSort = () => {
    let tempArray = gameData.slice();
    tempArray.sort(compare);

    setGameData(tempArray);
    setGameDataSecond(tempArray);
  };

  // input on chage handler
  const onChageInputText = (event) => {
    const searchData = gameDataSecond.filter((element) => {
      return element.title
        .toLowerCase()
        .includes(event.target.value.toLowerCase());
    });
    setGameData(searchData);
  };

  return (
    <div className="container border my-4 text-center">
      <div>
        <div className="container border my-4 text-center">
          <input
            onChange={onChageInputText}
            className="form-control my-4"
            placeholder="Type for Search"
            type="text"
          />
          <div onClick={onClickSort} className="container my-4">
            <button className="btn btn-success" type="button">
              {SORT_BUTTON_TEXT}
            </button>
          </div>
        </div>
        <Table data={gameData} />
      </div>
    </div>
  );
};

export default Home;
