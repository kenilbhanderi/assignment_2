import React from "react";
import { DATA_NOT_FOUND_ERROR } from "../../assets/constants/constants";

const Table = (props) => {

    const dataMapping =
    props.data.length === 0 ? (
      <tr>
        <td colSpan={6}>
          <h2>{DATA_NOT_FOUND_ERROR}</h2>
        </td>
      </tr>
    ) : (
        props.data.map((element) => {
        return (
          <tr key={element.id}>
            <td>{element.id}</td>
            <td>{element.title}</td>
            <td>{element.platform}</td>
            <td>{element.score}</td>
            <td>{element.genre}</td>
            <td>{element.editors_choice}</td>
          </tr>
        );
      })
    );

  return (
    <div className="container border my-4">
      <table className="table table-hover">
        <tbody>
          <tr>
            <th>No.</th>
            <th>Title</th>
            <th>Platform</th>
            <th>Score</th>
            <th>Genre</th>
            <th>Editors Choice</th>
          </tr>
          {dataMapping}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
