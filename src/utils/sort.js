export const compare = (a, b) => {
    if (a.platform.toLowerCase() < b.platform.toLowerCase()) {
      return -1;
    }
    if (a.platform.toLowerCase() > b.platform.toLowerCase()) {
      return 1;
    }
    return 0;
  };